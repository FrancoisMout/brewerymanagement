﻿namespace BreweryManagement.Models
{
    public class Stock
    {
        public int Id { get; set; }
        public int BeerId { get; set; }
        public int WholeSalerId { get; set; }
        public int Quantity { get; set; }
    }
}
