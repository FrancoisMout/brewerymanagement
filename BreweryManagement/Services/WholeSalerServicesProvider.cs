﻿using BreweryManagement.Contracts;
using BreweryManagement.Data;
using BreweryManagement.Models;
using BreweryManagement.Utils;
using Microsoft.EntityFrameworkCore;

namespace BreweryManagement.Services
{
    public class WholeSalerServicesProvider : IWholeSalerServicesProvider
    {
        private readonly DataContext _context;

        public WholeSalerServicesProvider(DataContext context)
        {
            _context = context;
        }

        public async Task<Result<AddBeersToWholeSalerRequest>> AddBeersAsync(AddBeersToWholeSalerRequest request)
        {
            if (request == null)
            {
                return Result<AddBeersToWholeSalerRequest>.Failure(ErrorMessages.RequestIsNull);
            }

            var wholeSaler = await _context.WholeSalers.FindAsync(request.WholeSalerId);

            if (wholeSaler == null)
            {
                return Result<AddBeersToWholeSalerRequest>.Failure(ErrorMessages.WholeSalerDoesNotExist);
            }

            var isBeerAlreadySoldByWholeSaler = await _context.Stocks.AnyAsync(s => s.WholeSalerId == request.WholeSalerId && s.BeerId == request.BeerId);
            if (isBeerAlreadySoldByWholeSaler)
            {
                return Result<AddBeersToWholeSalerRequest>.Failure(ErrorMessages.BeerAlreadySold);
            }

            var stock = new Stock
            {
                BeerId = request.BeerId,
                WholeSalerId = request.WholeSalerId,
                Quantity = request.Quantity
            };

            await _context.Stocks.AddAsync(stock);

            if (await _context.SaveChangesAsync() > 0)
            {
                return Result<AddBeersToWholeSalerRequest>.Success(request, SuccessMessages.StockWasAdded);
            }

            return Result<AddBeersToWholeSalerRequest>.Failure(ErrorMessages.FailedToAddBeer);
        }
    }
}
