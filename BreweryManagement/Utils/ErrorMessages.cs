﻿namespace BreweryManagement.Utils
{
    public static class ErrorMessages
    {
        public const string RequestIsNull = "The request is empty.";
        public const string BreweryDoesNotExist = "This brewery does not exist.";
        public const string PriceMustBePositive = "The price must be strictly positive.";
        public const string BeerAlreadyProduced = "This beer is already produced by the brewery.";
        public const string FailedToCreateBeer = "Failed to create this beer.";
        public const string WholeSalerDoesNotExist = "This wholesaler does not exist.";
        public const string BeerAlreadySold = "This beer is already sold by the wholesaler.";
        public const string FailedToAddBeer = "Failed to add this beer to wholesaler.";
    }
}
