﻿namespace BreweryManagement.Contracts
{
    public class BeerDetails
    {
        public string Name { get; set; }
        public double AlcoholLevel { get; set; }
        public double Price { get; set; }
        public BusinessDetails BreweryDetail { get; set; }
        public List<BusinessDetails> WholeSalersDetails { get; set; }
    }
}
