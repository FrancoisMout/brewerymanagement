﻿using BreweryManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace BreweryManagement.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options) { }

        public DbSet<Beer> Beers { get; set; }
        public DbSet<Brewery> Breweries { get; set; }
        public DbSet<WholeSaler> WholeSalers { get; set;}
        public DbSet<Stock> Stocks { get; set; }
    }
}
