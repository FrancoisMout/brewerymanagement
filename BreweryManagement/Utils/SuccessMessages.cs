﻿namespace BreweryManagement.Utils
{
    public static class SuccessMessages
    {
        public const string BeerWasCreated = "This beer was added successfully.";
        public const string StockWasAdded = "This beer was added successfully.";
    }
}
