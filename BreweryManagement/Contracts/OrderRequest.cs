﻿namespace BreweryManagement.Contracts
{
    public class OrderRequest
    {
        public List<BeerStock> Beers { get; set; }
    }
}
