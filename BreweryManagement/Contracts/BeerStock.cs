﻿namespace BreweryManagement.Contracts
{
    public class BeerStock
    {
        public int BeerId { get; set; }
        public int Quantity { get; set; }
    }
}
