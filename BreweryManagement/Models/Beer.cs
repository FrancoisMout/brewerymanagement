﻿namespace BreweryManagement.Models
{
    public class Beer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int BreweryId { get; set; }
        public double AlcoholLevel { get; set; }
        public double Price { get; set; }
    }
}
