﻿namespace BreweryManagement.Contracts
{
    public class UpdateBeerQuantityRequest
    {
        public int BeerId { get; set; }
        public int Quantity { get; set; }
    }
}
