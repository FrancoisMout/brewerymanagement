﻿namespace BreweryManagement.Contracts
{
    public class BusinessDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
