﻿using BreweryManagement.Contracts;
using BreweryManagement.Utils;

namespace BreweryManagement.Services
{
    public interface IWholeSalerServicesProvider
    {
        Task<Result<AddBeersToWholeSalerRequest>> AddBeersAsync(AddBeersToWholeSalerRequest request);
    }
}
