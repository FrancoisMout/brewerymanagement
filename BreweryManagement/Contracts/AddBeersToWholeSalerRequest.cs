﻿namespace BreweryManagement.Contracts
{
    public class AddBeersToWholeSalerRequest
    {
        public int WholeSalerId { get; set; }
        public int BeerId { get; set; }
        public int Quantity { get; set; }
    }
}
