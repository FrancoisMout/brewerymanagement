﻿using System.ComponentModel.DataAnnotations;

namespace BreweryManagement.Models
{
    public class Brewery
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
