﻿using BreweryManagement.Contracts;
using BreweryManagement.Controllers;
using BreweryManagement.Services;
using BreweryManagement.Utils;
using FizzWare.NBuilder;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace BreweryManagement.Test.Controllers
{
    public class BreweryControllerTest
    {
        private readonly Mock<IBreweryServicesProvider> servicesProviderMock = new();

        [Fact]
        public async Task AddBeerAsync_ShouldCallProviderAndReturnOk()
        {
            // Arrange
            var expected = GetSuccessResult();
            var request = GetRequest();            
            var controller = new BreweryController(servicesProviderMock.Object);

            servicesProviderMock
                .Setup(x => x.AddBeerAsync(It.IsAny<CreateBeerRequest>()))
                .ReturnsAsync(expected);

            // Act
            var result = await controller.AddBeerAsync(request).ConfigureAwait(false);

            // Assert
            result.GetType().Should().Be(typeof(OkObjectResult));
            servicesProviderMock.Verify(x => x.AddBeerAsync(request), Times.Once);
        }

        private static Result<CreateBeerRequest> GetSuccessResult()
        {
            return Builder<Result<CreateBeerRequest>>.CreateNew()
                .With(x => x.IsSuccess, true)
                .And(x => x.Value, GetRequest())
                .Build();
        }

        private static CreateBeerRequest GetRequest()
        {
            return Builder<CreateBeerRequest>.CreateNew().Build();
        }
    }
}
