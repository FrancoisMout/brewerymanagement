﻿namespace BreweryManagement.Contracts
{
    public class CreateBeerRequest
    {
        public int BreweryId { get; set; }
        public string BeerName { get; set; }
        public double AlcoholLevel { get; set; }
        public double Price { get; set; }
    }
}
