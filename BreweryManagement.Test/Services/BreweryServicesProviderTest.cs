using BreweryManagement.Contracts;
using BreweryManagement.Data;
using BreweryManagement.Models;
using BreweryManagement.Services;
using BreweryManagement.Utils;
using FizzWare.NBuilder;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;

namespace BreweryManagement.Test.Services
{
    public class BreweryServicesProviderTest : IAsyncLifetime
    {
        private const int BreweryId = 1;
        private const int BreweryIdNotExisting = -1;
        private const int AlcoholLevel = 2;
        private const int CorrectPrice = 3;
        private const int IncorrectPrice = 0;
        private const string BreweryName = "Brewery 1";
        private const string BeerName1 = "Beer 1";
        private const string BeerName2 = "Beer 2";

        private DataContext databaseContext;

        public async Task InitializeAsync()
        {
            databaseContext = await GetDataContext().ConfigureAwait(false);
        }

        [Fact]
        public async Task AddBeer_ShouldReturnIsSuccess()
        {
            // Arrange
            var serviceProvider = new BreweryServicesProvider(databaseContext);
            var request = GetCreateBeerRequest();

            // Act
            var result = await serviceProvider.AddBeerAsync(request).ConfigureAwait(false);

            // Assert
            result.IsSuccess.Should().BeTrue();
        }

        [Fact]
        public async Task AddBeer_ShouldReturnErrorMessage_WhenBreweryDoesNotExist()
        {
            // Arrange
            var serviceProvider = new BreweryServicesProvider(databaseContext);
            var request = GetCreateBeerRequest_WithNonExistingBrewery();
            var expectedMessage = ErrorMessages.BreweryDoesNotExist;

            // Act
            var result = await serviceProvider.AddBeerAsync(request).ConfigureAwait(false);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Error.Should().Be(expectedMessage);
        }

        [Fact]
        public async Task AddBeer_ShouldReturnErrorMessage_WhenPriceIsNotPositive()
        {
            // Arrange
            var serviceProvider = new BreweryServicesProvider(databaseContext);
            var request = GetCreateBeerRequest_WithIncorrectPrice();
            var expectedMessage = ErrorMessages.PriceMustBePositive;

            // Act
            var result = await serviceProvider.AddBeerAsync(request).ConfigureAwait(false);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Error.Should().Be(expectedMessage);
        }

        [Fact]
        public async Task AddBeer_ShouldReturnErrorMessage_WhenBeerAlreadyExist()
        {
            // Arrange
            var serviceProvider = new BreweryServicesProvider(databaseContext);
            var request = GetCreateBeerRequest_WithExistingBeer();
            var expectedMessage = ErrorMessages.BeerAlreadyProduced;

            // Act
            var result = await serviceProvider.AddBeerAsync(request).ConfigureAwait(false);

            // Assert
            result.IsSuccess.Should().BeFalse();
            result.Error.Should().Be(expectedMessage);
        }

        private static async Task<DataContext> GetDataContext()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "BreweryDatabaseTest")
                .Options;

            var databaseContext = new DataContext(options);

            databaseContext.Database.EnsureCreated();

            if (!await databaseContext.Breweries.AnyAsync())
            {
                databaseContext.Breweries.AddRange(GetBreweries());

                databaseContext.Beers.AddRange(GetBeers());
            }

            await databaseContext.SaveChangesAsync();

            return databaseContext;
        }

        private static IList<Brewery> GetBreweries()
        {
            return Builder<Brewery>.CreateListOfSize(1)
                .TheFirst(1)
                .With(x => x.Name, BreweryName)
                .And(x => x.Id, BreweryId)
                .Build();
        }

        private static IList<Beer> GetBeers()
        {
            return Builder<Beer>.CreateListOfSize(1)
                .TheFirst(1)
                .With(x => x.Name, BeerName1)
                .And(x => x.BreweryId, BreweryId)
                .Build();
        }

        private static CreateBeerRequest GetCreateBeerRequest()
        {
            return Builder<CreateBeerRequest>.CreateNew()
                .With(x => x.BreweryId, BreweryId)
                .And(x => x.BeerName, BeerName2)
                .And(x => x.AlcoholLevel, AlcoholLevel)
                .And(x => x.Price, CorrectPrice)
                .Build();
        }


        private static CreateBeerRequest GetCreateBeerRequest_WithNonExistingBrewery()
        {
            return Builder<CreateBeerRequest>.CreateNew()
                .With(x => x.BreweryId, BreweryIdNotExisting)
                .And(x => x.BeerName, BeerName2)
                .And(x => x.AlcoholLevel, AlcoholLevel)
                .And(x => x.Price, CorrectPrice)
                .Build();
        }

        private static CreateBeerRequest GetCreateBeerRequest_WithIncorrectPrice()
        {
            return Builder<CreateBeerRequest>.CreateNew()
                .With(x => x.BreweryId, BreweryId)
                .And(x => x.BeerName, BeerName2)
                .And(x => x.AlcoholLevel, AlcoholLevel)
                .And(x => x.Price, IncorrectPrice)
                .Build();
        }

        private static CreateBeerRequest GetCreateBeerRequest_WithExistingBeer()
        {
            return Builder<CreateBeerRequest>.CreateNew()
                .With(x => x.BreweryId, BreweryId)
                .And(x => x.BeerName, BeerName1)
                .And(x => x.AlcoholLevel, AlcoholLevel)
                .And(x => x.Price, CorrectPrice)
                .Build();
        }

        public async Task DisposeAsync()
        {
            await databaseContext.DisposeAsync();
        }
    }
}