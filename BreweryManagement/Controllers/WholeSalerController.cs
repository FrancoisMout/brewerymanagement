﻿using BreweryManagement.Contracts;
using BreweryManagement.Services;
using Microsoft.AspNetCore.Mvc;

namespace BreweryManagement.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WholeSalerController : DefaultController
    {
        private readonly IWholeSalerServicesProvider _servicesProvider;

        public WholeSalerController(IWholeSalerServicesProvider servicesProvider)
        {
            _servicesProvider = servicesProvider;
        }

        [HttpPut("addBeer")]
        public async Task<IActionResult> AddBeersAsync(AddBeersToWholeSalerRequest request)
        {
            var result = await _servicesProvider.AddBeersAsync(request);
            return HandleResultWithMessage(result);
        }
    }
}
