﻿using BreweryManagement.Contracts;
using BreweryManagement.Data;
using BreweryManagement.Models;
using BreweryManagement.Utils;
using Microsoft.EntityFrameworkCore;

namespace BreweryManagement.Services
{
    public class BreweryServicesProvider : IBreweryServicesProvider
    {
        private readonly DataContext _context;

        public BreweryServicesProvider(DataContext context)
        {
            _context = context;
        }

        public async Task<Result<CreateBeerRequest>> AddBeerAsync(CreateBeerRequest request)
        {
            if (request == null)
            {
                return Result<CreateBeerRequest>.Failure(ErrorMessages.RequestIsNull);
            }

            var brewery = await _context.Breweries.FindAsync(request.BreweryId);

            if (brewery == null)
            {
                return Result<CreateBeerRequest>.Failure(ErrorMessages.BreweryDoesNotExist);
            }

            if (request.Price <= 0)
            {
                return Result<CreateBeerRequest>.Failure(ErrorMessages.PriceMustBePositive);
            }

            var isBeerAlreadyProduced = await _context.Beers.AnyAsync(b => b.BreweryId == request.BreweryId && b.Name == request.BeerName);
            if (isBeerAlreadyProduced)
            {
                return Result<CreateBeerRequest>.Failure(ErrorMessages.BeerAlreadyProduced);
            }

            var beer = new Beer
            {
                Name = request.BeerName,
                AlcoholLevel = request.AlcoholLevel,
                Price = request.Price,
                BreweryId = request.BreweryId,
            };

            await _context.Beers.AddAsync(beer);

            if (await _context.SaveChangesAsync() > 0)
            {
                return Result<CreateBeerRequest>.Success(request, SuccessMessages.BeerWasCreated);
            }

            return Result<CreateBeerRequest>.Failure(ErrorMessages.FailedToCreateBeer);
        }
    }
}
