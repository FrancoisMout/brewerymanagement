﻿namespace BreweryManagement.Contracts
{
    public class OrderResponse
    {
        public BusinessDetails WholeSalerDetails { get; set; }
        public DateTime DateOfOrder { get; set; }
        public double Price { get; set; }
        public List<BeerStock> Beers { get; set; }
    }
}
