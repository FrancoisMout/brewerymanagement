﻿namespace BreweryManagement.Contracts
{
    public class RetrieveBeersResult
    {
        public List<BeerDetails> AllBeers { get; set; }
    }
}
