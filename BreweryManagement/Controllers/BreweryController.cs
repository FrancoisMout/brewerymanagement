﻿using BreweryManagement.Contracts;
using BreweryManagement.Services;
using Microsoft.AspNetCore.Mvc;

namespace BreweryManagement.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BreweryController : DefaultController
    {
        private readonly IBreweryServicesProvider _servicesProvider;

        public BreweryController(IBreweryServicesProvider servicesProvider)
        {
            _servicesProvider = servicesProvider;
        }

        [HttpPut("addBeer")]
        public async Task<IActionResult> AddBeerAsync(CreateBeerRequest request)
        {
            var result = await _servicesProvider.AddBeerAsync(request);
            return HandleResultWithMessage(result);
        }
    }
}
