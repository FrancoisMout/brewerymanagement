﻿using BreweryManagement.Contracts;
using BreweryManagement.Utils;

namespace BreweryManagement.Services
{
    public interface IBreweryServicesProvider
    {
        Task<Result<CreateBeerRequest>> AddBeerAsync(CreateBeerRequest request);
    }
}
